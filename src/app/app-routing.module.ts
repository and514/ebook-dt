import { NgModule } from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {HomeComponent} from "./home/home.component";
import {ChoiceFolderComponent} from "./choice-folder/choice-folder.component";
import {BooksComponent} from "./books/books.component";


// http://localhost:4200/ -> HomeComponent
// http://localhost:4200/choice_folders -> ChoiceFolderComponent
const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'choice_folders', component: ChoiceFolderComponent},
  {path: 'books', component: BooksComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
