import { Component, ElementRef, ViewChild } from '@angular/core';

export interface Folder {
  name: string;
  path: string;
  count: number;
}

@Component({
  selector: 'app-choice-folder',
  templateUrl: './choice-folder.component.html',
  styleUrls: ['./choice-folder.component.css']
})
export class ChoiceFolderComponent {
  public folder: string = '';
  public folderList: Folder[] = [];

  // @ts-ignore
  @ViewChild("folder_name") elementLoadFolder: ElementRef;

  // constructor(
  //   private _fb: FormBuilder,
  //   private _authService: AuthService,
  //   private _globalService: GlobalService,
  //   private _api: ApiService,
  //   private _authUrlsService: AuthUrlsService,
  // ) {
  // }

  uploadFolder() {
    this.elementLoadFolder.nativeElement.click();
  }

  onFListChange(event){
    let files = event.target.files;
    let file_path = files ? files[0] : '';
    this.folder = file_path.path.split('/').slice(0, -1).join('/');
  }
}
