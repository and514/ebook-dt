// Modules to control application life and create native browser window
const { app, BrowserWindow } = require('electron');
const path = require('path');
const url = require("url");

// Храните глобальную ссылку на объект окна, если вы этого не сделаете, окно будет
// автоматически закрываться, когда объект JavaScript собирает мусор.
let win;

function createWindow () {
  // Создаём окно браузера.
  win = new BrowserWindow({
    width: 800,
    height: 600,
    backgroundColor: '#63ffcf',
    // icon: `file://${__dirname}/ebook-dt/assets/ico/favicon.ico`

    // webPreferences: {
    //  nodeIntegration: false
    //  nodeIntegration: true
    //   // preload: path.join(__dirname, 'preload.js')
    // }

    // frame:false,
    // toolbar: false,
    // 'accept-first-mouse': true,
    // transparent: true,
    // left:d.workArea.x,
    // top:d.workArea.y,
    // 'skip-taskbar': true,
    // 'auto-hide-menu-bar': true,
    // 'enable-larger-than-screen': true,
    // width:size.width,
    // height:size.height

  });
  // win.setMenuBarVisibility(false);
  win.autoHideMenuBar = true;

  // и загрузить index.html приложения.
  win.loadURL(
    url.format({
      // тут нужно уточнить, что путь к файлу index.html будет валиден для
      // Angular 6+ приложения. Если у вас версия ниже, то используйте
      // следующий путь - /dist/index.html
      pathname: path.join(__dirname, '/ebook-dt/index.html'),
      protocol: "file:",
      slashes: true
    })
  );

  // Отображаем средства разработчика.
 win.webContents.openDevTools();

  // Будет вызвано, когда окно будет закрыто.
  win.on('closed', () => {
    // Разбирает объект окна, обычно вы можете хранить окна
    // в массиве, если ваше приложение поддерживает несколько окон в это время,
    // тогда вы должны удалить соответствующий элемент.
    win = null;
  });

}

// Этот метод будет вызываться, когда Electron закончит
// инициализацию и готов к созданию окон браузера.
// Некоторые API могут использоваться только после возникновения этого события.
app.on('ready', createWindow);

// Выходим, когда все окна будут закрыты.
app.on('window-all-closed', () => {
  // Для приложений и строки меню в macOS является обычным делом оставаться
  // активными до тех пор, пока пользователь не выйдет окончательно используя Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  // На MacOS обычно пересоздают окно в приложении,
  // после того, как на иконку в доке нажали и других открытых окон нету.
  if (win === null) {
    createWindow();
  }
});

// В этом файле вы можете включить код другого основного процесса
// вашего приложения. Можно также поместить их в отдельные файлы и применить к ним require.


